#ifndef __NIVEAU__
#define __NIVEAU__

#include "structures.h"
/* Fonctions */

/***********************************
 * Nom : createPlatform
 *
 * Objectif : initialise une plateforme
 *
 * Paramètres :
 *  - player : le joueur
 *  - x, y les coordonnées de la plateforme
 *  - longueur, hauteur les dimmensions de la plateforme
 *  - option : 0 plateforme fixe, 1 plateforme mouvante, 2 plateforme déplaçable
 *  - deplacement_x, deplacement_y : le déplacement de la plateforme si option 1;
 *  - speep : la vitesse de la plateforme si option 1
 *  - sprite : le sprite de la plateforme
 *  - display, plan
 *
 * Retour :
 *  - la plateforme
 *
***********************************/

Element * createPlatform(Element * player, int x, int y, int longueur, int hauteur,int option,int deplacement_x, int deplacement_y, int speed, char * sprite, int display, int plan);

/***********************************
 * Nom : mooving_platform
 *
 * Objectif : plateforme mouvante
 *
 * Paramètres :
 *  - platform : la plateforme
 *
 * Retour :
 *  - void
 *
***********************************/

void mooving_platform(Element * platform);

/***********************************
 * Nom : interactive_platform
 *
 * Objectif : plateforme déplaçable
 *
 * Paramètres :
 *  - platform : la plateforme
 *
 * Retour :
 *  - void
 *
***********************************/

void interactive_platform(Element * platform);


//ne fonctionne pas
void createDoor(Element * player, int xdepart, int ydepart, int xarrive, int yarrive, int longueur, int hauteur, int displayDepart, int planDepart, int displayArrive, int planArrive, char * sprite);

void tp(Element * door);

#endif
