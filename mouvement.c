#include "mouvement.h"

Element * initPlayer(int x, int y, int largeur, int hauteur, char * sprite, int display, int plan, int gravite,int wallJump, int speed, int hauteur_saut){

  Element * player = createImage(x,y,largeur,hauteur,sprite,display,plan);
  perso_t * structPerso = initDataPerso(speed,gravite,hauteur_saut);

  if(player && structPerso){
    setDataElement(player,(void*)structPerso);

    if(!gravite){

      setKeyPressedElement(player,key_press_nograv);
      setKeyReleasedElement(player,key_release_nograv);
      setActionElement(player,movePlayerNograv);

    }else if(gravite > 0){

      setKeyPressedElement(player,key_press_grav);
      setKeyReleasedElement(player,key_release_grav);

      if(wallJump){
        setActionElement(player,movePlayerGravWallJump);
      }else{
        setActionElement(player,movePlayerGravNoWallJump);
      }

    }


  }

  return player;

}

void key_press_nograv(Element * player, SDL_Keycode c){

  perso_t * structPerso;

  getDataElement(player,(void**)&structPerso);

  switch (c) {
    //direction x
    case 'q':
    {
      structPerso->left=1;
      setAnimationElement(player,ANIM_LEFT);
      setWaySpriteAnimationElement(player,ANIM_LEFT,1);
      break;
    }
    case 'd':
    {
      structPerso->right=1;
      setAnimationElement(player,ANIM_RIGHT);
      setWaySpriteAnimationElement(player,ANIM_RIGHT,1);
      break;
    }
    //direction y
    case 'z':
    {
      structPerso->up=1;
      setAnimationElement(player,ANIM_UP);
      setWaySpriteAnimationElement(player,ANIM_UP,1);
      break;
    }
    case 's':
    {
      structPerso->down=1;
      setAnimationElement(player,ANIM_DOWN);
      setWaySpriteAnimationElement(player,ANIM_DOWN,1);
      break;
    }
    case 'e':
    {
      if(structPerso->action == 0){

        structPerso->action=1;

      }
      break;
    }
  }
}

void key_release_nograv(Element * player, SDL_Keycode c){

  perso_t * structPerso;

  getDataElement(player,(void**)&structPerso);

  switch (c) {
    //direction x
    case 'q':
    {
      structPerso->left=0;
      setSpriteAnimationElement(player,ANIM_LEFT);
      setWaySpriteAnimationElement(player,ANIM_LEFT,0);
      break;
    }
    case 'd':
    {
      structPerso->right=0;
      setSpriteAnimationElement(player,ANIM_RIGHT);
      setWaySpriteAnimationElement(player,ANIM_RIGHT,0);
      break;
    }
    //direction y
    case 'z':
    {
      structPerso->up=0;
      setSpriteAnimationElement(player,ANIM_UP);
      setWaySpriteAnimationElement(player,ANIM_UP,0);
      break;
    }
    case 's':
    {
      structPerso->down=0;
      setSpriteAnimationElement(player,ANIM_DOWN);
      setWaySpriteAnimationElement(player,ANIM_DOWN,0);
      break;
    }
    case 'e':
    {
      structPerso->action=0;
      break;
    }
  }
}

void movePlayerNograv(Element * player){

  perso_t * structPerso;
  float xplayer, yplayer, wplayer, hplayer;
  float xwall, ywall, wwall, hwall;
  Element * cour;
  int collision = 0;

  getDataElement(player,(void**)&structPerso);

  getCoordElement(player,&xplayer,&yplayer);
  getDimensionElement(player,&wplayer,&hplayer);

  initIteratorElement(player);

  while((cour = nextIteratorElement(player))){

    if(structPerso->sac->objet != cour){

      getCoordElement(cour,&xwall,&ywall);
      getDimensionElement(cour,&wwall,&hwall);

      collision = (xplayer + wplayer + structPerso->speed*(structPerso->right-structPerso->left) >= xwall && xplayer + structPerso->speed*(structPerso->right-structPerso->left) <= xwall + wwall && yplayer + hplayer + structPerso->speed*(structPerso->down-structPerso->up) >= ywall && yplayer + structPerso->speed*(structPerso->down-structPerso->up) <= ywall + hwall);

      if(collision){

        if(yplayer + hplayer < ywall){
          structPerso->down=0;
        }
        if(xplayer + wplayer < xwall){
          structPerso->right=0;
        }
        if(xplayer > xwall + wwall){
          structPerso->left=0;
        }
        if(yplayer > ywall + hwall){
          structPerso->up=0;
        }

      }

    }

  }

  moveElement(player,structPerso->speed*(structPerso->right-structPerso->left),structPerso->speed*(structPerso->down-structPerso->up));

  if(structPerso->sac->objet){

      moveElement(structPerso->sac->objet,structPerso->speed*(structPerso->right-structPerso->left),structPerso->speed*(structPerso->down-structPerso->up));

  }
}

void key_press_grav(Element * player, SDL_Keycode c){

  perso_t * structPerso;

  getDataElement(player,(void**)&structPerso);

  switch (c) {
    //direction x
    case 'q':
    {
      structPerso->left=1;
      setSpriteAnimationElement(player,ANIM_LEFT);
      setWaySpriteAnimationElement(player,ANIM_LEFT,1);
      break;
    }
    case 'd':
    {
      structPerso->right=1;
      setSpriteAnimationElement(player,ANIM_RIGHT);
      setWaySpriteAnimationElement(player,ANIM_RIGHT,1);
      break;
    }
    //saut
    case 32:
    {
      if(!structPerso->jump){

        structPerso->jump=1;
        structPerso->dt=structPerso->gravite+structPerso->hauteur_saut;
        setSpriteAnimationElement(player,ANIM_JUMP);
        setWaySpriteAnimationElement(player,ANIM_JUMP,1);

      }
      break;
    }
    case 'e':
    {
      if(structPerso->action == 0){

        structPerso->action=1;

      }
      break;
    }
  }
}

void key_release_grav(Element * player, SDL_Keycode c){

  perso_t * structPerso;

  getDataElement(player,(void**)&structPerso);

  switch (c) {
    //direction x
    case 'q':
    {
      structPerso->left=0;
      setSpriteAnimationElement(player,ANIM_LEFT);
      setWaySpriteAnimationElement(player,ANIM_LEFT,0);
      break;
    }
    case 'd':
    {
      structPerso->right=0;
      setSpriteAnimationElement(player,ANIM_RIGHT);
      setWaySpriteAnimationElement(player,ANIM_RIGHT,0);
      break;
    }
    case 32:
    {
      setSpriteAnimationElement(player,ANIM_JUMP);
      setWaySpriteAnimationElement(player,ANIM_JUMP,0);
      break;
    }
    case 'e':
    {
      structPerso->action=0;
      break;
    }
  }
}

void movePlayerGravNoWallJump(Element * player){

  perso_t * structPerso;
  float xplayer, yplayer, wplayer, hplayer;
  float xwall, ywall, wwall, hwall;
  float xobjet, yobjet, wobjet, hobjet;
  Element * cour;
  int collision = 0,sol=0;
  platform_t * structPlatform;


  getCoordElement(player,&xplayer,&yplayer);
  getDimensionElement(player,&wplayer,&hplayer);

  initIteratorElement(player);
  getDataElement(player,(void**)&structPerso);

  while((cour = nextIteratorElement(player))){

    if(structPerso->sac->objet != cour){

      getCoordElement(cour,&xwall,&ywall);
      getDimensionElement(cour,&wwall,&hwall);
      getDataElement(cour,(void **)&structPlatform);

      collision = (xplayer + wplayer + structPerso->speed*(structPerso->right-structPerso->left) >= xwall && xplayer + structPerso->speed*(structPerso->right-structPerso->left) <= xwall + wwall && yplayer + hplayer + structPerso->gravite-structPerso->dt >= ywall && yplayer + structPerso->gravite-structPerso->dt <= ywall + hwall);

      if(collision){

        if(yplayer + hplayer < ywall){
          sol=1;
        }
        if(xplayer + wplayer < xwall){
          structPerso->right=0;
        }
        if(xplayer > xwall + wwall){
          structPerso->left=0;
        }
        if(yplayer > ywall + hwall){
          structPerso->dt=0;
        }

        if(xplayer + wplayer >= xwall && xplayer <= xwall + wwall && yplayer + hplayer >= ywall && yplayer <= ywall + hwall){

          replaceElement(player,xplayer,ywall-hplayer);

          if(structPerso->sac->objet){

            getCoordElement(structPerso->sac->objet,&xobjet,&yobjet);
            getDimensionElement(structPerso->sac->objet,&wobjet,&hobjet);

            replaceElement(structPerso->sac->objet,xobjet,ywall-hobjet);
          }

        }
      }

    }


  }



  if(!sol){

    //printf("%d,%d\n",collision,structPerso->dt);
    moveElement(player,structPerso->speed*(structPerso->right-structPerso->left),structPerso->gravite-structPerso->dt);

    if(structPerso->sac->objet){

      moveElement(structPerso->sac->objet,structPerso->speed*(structPerso->right-structPerso->left),structPerso->gravite-structPerso->dt);

    }

  }else{
        //printf("%d,%d\n",collision,structPerso->dt);
        structPerso->jump = 0;
        moveElement(player,structPerso->speed*(structPerso->right-structPerso->left),-structPerso->dt);

        if(structPerso->sac->objet){

          moveElement(structPerso->sac->objet,structPerso->speed*(structPerso->right-structPerso->left),-structPerso->dt);

        }

  }

  if(structPerso->dt>0){

    structPerso->dt--;

  }

}

void movePlayerGravWallJump(Element * player){

  perso_t * structPerso;
  float xplayer, yplayer, wplayer, hplayer;
  float xwall, ywall, wwall, hwall;
  float xobjet, yobjet, wobjet, hobjet;
  Element * cour;
  int collision = 0,sol=0;


  getCoordElement(player,&xplayer,&yplayer);
  getDimensionElement(player,&wplayer,&hplayer);
  getDataElement(player,(void**)&structPerso);

  initIteratorElement(player);

  while((cour = nextIteratorElement(player))){

    if(structPerso->sac->objet != cour){

      getCoordElement(cour,&xwall,&ywall);
      getDimensionElement(cour,&wwall,&hwall);

      collision = (xplayer + wplayer + structPerso->speed*(structPerso->right-structPerso->left) >= xwall && xplayer + structPerso->speed*(structPerso->right-structPerso->left) <= xwall + wwall && yplayer + hplayer + structPerso->gravite-structPerso->dt >= ywall && yplayer + structPerso->gravite-structPerso->dt <= ywall + hwall);

      if(collision){

        if(yplayer + hplayer < ywall){
          sol=1;
        }
        if(xplayer + wplayer < xwall){
          structPerso->right=0;
          sol=1;
        }
        if(xplayer > xwall + wwall){
          structPerso->left=0;
          sol=1;
        }
        if(yplayer > ywall + hwall){
          structPerso->dt=0;
        }

        if(xplayer + wplayer >= xwall && xplayer <= xwall + wwall && yplayer + hplayer >= ywall && yplayer <= ywall + hwall){

          replaceElement(player,xplayer,ywall-hplayer);

          if(structPerso->sac->objet){

            getCoordElement(structPerso->sac->objet,&xobjet,&yobjet);
            getDimensionElement(structPerso->sac->objet,&wobjet,&hobjet);

            replaceElement(structPerso->sac->objet,xobjet,ywall-hobjet);
          }

        }
      }

    }

  }



  if(!sol){

    //printf("%d,%d\n",collision,structPerso->dt);
    moveElement(player,structPerso->speed*(structPerso->right-structPerso->left),structPerso->gravite-structPerso->dt);

    if(structPerso->sac->objet){

      moveElement(structPerso->sac->objet,structPerso->speed*(structPerso->right-structPerso->left),structPerso->gravite-structPerso->dt);

    }

  }else{
        //printf("%d,%d\n",collision,structPerso->dt);
        structPerso->jump = 0;
        moveElement(player,structPerso->speed*(structPerso->right-structPerso->left),-structPerso->dt);

        if(structPerso->sac->objet){

          moveElement(structPerso->sac->objet,structPerso->speed*(structPerso->right-structPerso->left),-structPerso->dt);

        }

  }

  if(structPerso->dt>0){

    structPerso->dt--;

  }

}

void setCollision(Element * player, Element * wall){

  addElementToElement(player,wall);

}

void addAnimation(Element * player, int nb_sprites, int x_sprite, int y_sprite, int largeur, int hauteur, int numAnim){

  addAnimationElement(player,numAnim);

  for(int k=0;k<nb_sprites;k++){

    addSpriteAnimationElement(player,numAnim,x_sprite+largeur*k,y_sprite,largeur,hauteur,5,k);

  }

  setWaySpriteAnimationElement(player,numAnim,0);


}
