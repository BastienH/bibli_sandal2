#ifndef __STRUCTURES__
#define __STRUCTURES__

#include <SANDAL2/SANDAL2.h>
#include <SDL2/SDL_mixer.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef enum AnimDirection
{
  ANIM_RIGHT,
  ANIM_LEFT,
  ANIM_UP,
  ANIM_DOWN,
  ANIM_JUMP
}AnimDirection;
/* Structures */

/***********************************
 * Nom :
 *
 * Contenu :
 *  -
 *  -
 *  -
 *  -
 *  -
 *
 ***********************************/

typedef enum defaultPlan
{
  BUTTON_ON_p = -100
}defaultPlan;

/***********************************
* Nom : perso
*
* Contenu :
*  - up, down, right, left, jump des booléens utilisés pour le mouvement
*  - gravite
*  - speed la vitesse de déplacement du personnage
*  - dt
*  - hauteur_saut la hauteur du saut
*  - action : booléen, si le personnage execute une action ou non
*  - sac : l'inventaire du personnage
*
***********************************/
typedef struct inventaire{
  Element * objet;
}inventaire_t;

typedef struct perso{
 int up;
 int left;
 int down;
 int right;
 int gravite;
 int speed;
 int dt;
 int jump;
 int hauteur_saut;
 int action;
 inventaire_t * sac;
}perso_t;



perso_t * initDataPerso(int speed, int gravite, int hauteur_saut);

typedef struct button_t
{
 int colorClique[4];
 int option;
 int param;
 int display;
 char * source;
 char * txt;
 int colorBloc[4];
}button_t;

button_t * initDataButton(int colorClique[4], int option, int param, int display, char * source, char * txt, int colorBloc[4]);

void showbt(button_t * databt);

/***********************************
* Nom : platform
*
* Contenu :
*  - xdepart, xarrive, ydepart, yarrive les coordonnées de départ et arrivé de la plateforme
*  - speed : la vitesse de la plateforme
*
***********************************/

typedef struct platform{
 Element * player;
 int xdepart;
 int xarrive;
 int ydepart;
 int yarrive;
 int speed;
}platform_t;

platform_t * initDataPlatform(Element * player, int speed, int xdepart, int ydepart, int xarrive, int yarrive);

/***********************************
* Nom : bar_t
*
* Contenu :
*  - val_max : la valeur maximale
*  - val_actuelle : la valeur actuelle
*  - prefixe : information de la barre (ex : "hp :")
*  - texte : le texte affiché sur la barre
*  - bar : la barre à modifier
*
***********************************/

typedef struct{
 int val_max;
 int val_actuelle;
 char * prefixe;
 Element * texte;
 Element * bar;
}bar_t;

typedef struct{
  int displayArrive;
  int xarrive;
  int yarrive;
  Element * player;
}door_t;


#endif
