#ifndef __MOUVEMENT__
#define __MOUVEMENT__

#include "structures.h"
/* Fonctions */

/***********************************
 * Nom : initPlayer
 *
 * Objectif : initialise un joueur
 *
 * Paramètres :
 *  - x, y les coordonnées du joueur
 *  - largeur, hauteur les dimmensions du joueur
 *  - sprite le fichier contenant le sprite ou le spritesheet si le joueur est animé
 *  - display, plan
 *  - gravite : 0 si aucune, >0 sinon
 *  - wallJump : 1 si activé, 0 sinon
 *  - speed la vitesse du joueur
 *  - hauteur_saut la hauteur du saut voulue si gravite présente
 *
 * Retour :
 *  - le personnage
 *
***********************************/

Element * initPlayer(int x, int y, int largeur, int hauteur, char * sprite, int display, int plan, int gravite, int wallJump, int speed, int hauteur_saut);

//mouvement sans gravite

/***********************************
 * Nom : key_press_nograv, key_release_nograv
 *
 * Objectif : gérer les inputs claviers pour le mouvement
 *
 * Paramètres :
 *  - player, le joueur
 *  - c, la touche qui est appuyée (resp relachée)
 *
 * Retour : void
 *
***********************************/

void key_press_nograv(Element * player, SDL_Keycode c);

void key_release_nograv(Element * player, SDL_Keycode c);

/***********************************
 * Nom : movePlayerNograv
 *
 * Objectif : gérer le déplacement du personnage sans gravité
 *
 * Paramètres :
 *  - player, le joueur
 *
 * Retour : void
 *
***********************************/

void movePlayerNograv(Element * player);

//mouvement avec gravite

/***********************************
 * Nom : key_press_grav, key_release_grav
 *
 * Objectif : gérer les inputs claviers pour le mouvement
 *
 * Paramètres :
 *  - player, le joueur
 *  - c, la touche qui est appuyée (resp relachée)
 *
 * Retour : void
 *
***********************************/

void key_press_grav(Element * player, SDL_Keycode c);

void key_release_grav(Element * player, SDL_Keycode c);

/***********************************
 * Nom : movePlayerGrav
 *
 * Objectif : gérer le déplacement du personnage avec gravité, avec ou sans wallJump
 *
 * Paramètres :
 *  - player, le joueur
 *
 * Retour : void
 *
***********************************/

void movePlayerGravNoWallJump(Element * player);

void movePlayerGravWallJump(Element * player);

//autres fonctions

/***********************************
 * Nom : setCollision
 *
 * Objectif : ajoute une collision entre player et wall
 *
 * Paramètres :
 *  - player, le joueur
 *  - wall, l'onjet avec lequel il va collisionner
 *
 * Retour : void
 *
***********************************/

void setCollision(Element * player, Element * wall);

/***********************************
 * Nom : addAnimation
 *
 * Objectif : ajoute une animation au joueur
 *
 * Paramètres :
 *  - player, le joueur
 *  - nb_sprites le nombre de sprites de l'animation
 *  - x_sprite, y_sprite, largeur, hauteur les coordonnées et dimmensions du sprite dans le spritesheet
 *  - numAnim le numéro de l'animation
 *
 * Retour : void
 *
***********************************/

void addAnimation(Element * player, int nb_sprites, int x_sprite, int y_sprite, int largeur, int hauteur, int numAnim);

#endif
