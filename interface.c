#include "interface.h"



Element * initBar(int x, int y, int longueur, int hauteur, int valeur_max, int valeur_initiale, char * texte, int display, int plan){

  bar_t * barre = malloc(sizeof(bar_t));
  int red[4] = {255,0,0,0};
  int black[4] = {0,0,0,0};
  int white[4] = {255,255,255,0};
  int marge = 5;
  char nombre[50], temp[50];
  Element * fond = createBlock(x, y, longueur, hauteur, white, display, plan);

  if(barre){

    sprintf(nombre,"%d",valeur_initiale);
    strcat(temp,texte);
    strcat(temp,nombre);

    barre->bar =  createBlock(x + marge, y + marge, (float)valeur_initiale/valeur_max*(longueur - 2*marge) , hauteur - 2*marge, red, display, plan-1);
    barre->val_max = valeur_max;
    barre->val_actuelle = valeur_initiale;
    barre->texte = createText(x + marge, y + marge, longueur - 2*marge , hauteur - 2*marge, 90, "Font/arial.ttf", temp, black, SANDAL2_BLENDED, display, plan-2);
    barre->prefixe = malloc((strlen(texte)+1)*sizeof(char));
    strcpy(barre->prefixe, texte);
    setDataElement(fond,(void *)barre);

  }

  return fond;
}


void majBar(Element * fond, int changement, int display, int plan){

  bar_t * barre;
  char nombre[50], temp[50];
  int marge = 5;
  float x, y, longueur, hauteur;
  int red[4] = {255,0,0,0};
  int black[4] = {0,0,0,0};

  getDataElement(fond,(void **)&barre);
  getCoordElement(fond, &x, &y);
  getDimensionElement(fond, &longueur, &hauteur);

  if(changement<0){
    if(barre->val_actuelle + changement<0){
      barre->val_actuelle = 0;
    }else{
      barre->val_actuelle = barre->val_actuelle + changement;
    }
  }else{
    if(barre->val_actuelle + changement > barre->val_max){
      barre->val_actuelle = barre->val_max;
    }else{
      barre->val_actuelle = barre->val_actuelle + changement;
    }
  }

  sprintf(nombre,"%d",barre->val_actuelle);
  strcpy(temp,barre->prefixe);
  strcat(temp,nombre);

  if(barre->texte && barre->bar){

    delElement(barre->texte);
    delElement(barre->bar);

  }
  barre->bar = createBlock(x + marge, y + marge, (float)barre->val_actuelle/barre->val_max*(longueur - 2*marge) , hauteur - 2*marge, red, display, plan-1);
  barre->texte = createText(x + marge, y + marge, longueur - 2*marge , hauteur - 2*marge, 90, "Font/arial.ttf", temp, black, SANDAL2_BLENDED, display, plan-2);


}
