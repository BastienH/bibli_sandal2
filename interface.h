#ifndef __INTERFACE__
#define __INTERFACE__

#include "structures.h"

/* Fonctions */

/***********************************
 * Nom : initBar
 *
 * Objectif : création d'une barre
 *
 * Paramètres :
 *  - x, y, longueur, hauteur : les coordonnées et dimensions de la barre
 *  - valeur_max : la valeur maximale de la barre
 *  -  valeur_initiale : la valeur initiale de la barre
 *  - texte : information sur la barre
 *  - display, plan
 *
 * Retour :
 *  - la barre
 *
***********************************/

Element * initBar(int x, int y, int longueur, int hauteur, int valeur_max, int valeur_initiale, char * texte, int display, int plan);

/***********************************
 * Nom : majBar
 *
 * Objectif : mis à jour d'une barre
 *
 * Paramètres :
 *  - fond : la barre
 *  - changement : le changement à faire (ex : -5)
 *  - display, plan
 *
 * Retour :
 *  - void
 *
***********************************/

void majBar(Element * fond, int changement, int display, int plan);



#endif
