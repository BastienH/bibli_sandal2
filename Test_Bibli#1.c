#include "Bibli_Sandal2.h"

int main()
{
    int black[4] = {0,0,0,0}; // couleur du fond de la fenêtre principale
    int white[4] = {255,255,255,0};
    int green[4] = {0,255,0,0};
    ZIK * mus = NULL; // lecteur de musiques
    Element * player;
    Element * wall;
    Element * bar;

	if(!initGame(600,600,"TEST BIBLI_SANDAL2",black,1)) // initialise la Sandal, SDL_mixer et créé une fenêtre noire
	                                                    // de taille 600 par 600 nommée TEST BIBLI_SANDAL2
	                                                    // Mettre un 0 à la place du 1 s'il n'y a pas de musique dans le jeu
	{
		mus = initZIK("Music/liste_musique"); // on initialise le lecteur avec le fichier contenant la liste des musiques
		goZIK(mus,0); // on lance la première musique

    initButton(10,10,1,"Ceci est une sandal",white,green,black,"Font/arial.ttf",1,1,0,0);
    // InitButton(position en x, en y, taille, texte, couleur du texte, couleur quand cliqué, fond, source, option, param, display, plan)

    player = initPlayer(150,0,100,100,"Asset/sandal2.png",0,0,4,1,5,20);

    //mur fixe
    wall = createPlatform(player,0,300,300,100,0,0,0,0,"Asset/sandal2.png",0,0);
    setCollision(player,wall);
    //mur fixe
    wall = createPlatform(player,0,0,50,300,0,0,0,0,"Asset/sandal2.png",0,0);
    setCollision(player,wall);
    //mur fixe
    wall = createPlatform(player,0,0,100,20,0,0,0,0,"Asset/sandal2.png",0,0);
    setCollision(player,wall);
    //mur mouvant
    wall = createPlatform(player,100,100,100,10,1,300,100,2,"Asset/sandal2.png",0,0);
    setCollision(player,wall);
    //mur déplaçable
    wall = createPlatform(player,200,250,50,50,2,0,0,0,"Asset/sandal2.png",0,0);
    setCollision(player,wall);


    bar = initBar(400, 0, 200, 50, 365, 14, "vie : ", 0, 0);
    majBar(bar,+100,0,0);

    //createDoor(player, 500, 500, 500, 500, 100, 20, 0, 0, 1, 0, "Asset/sandal2.png");

		while(!runGame(16)); // on fait tourner le jeu
    libereZIK(mus); // on libère le lecteur
		endGame(); // on met fin au jeu
	}

  return 0;
}
