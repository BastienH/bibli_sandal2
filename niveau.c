#include "niveau.h"

Element * createPlatform(Element * player, int x, int y, int longueur, int hauteur,int option,int deplacement_x, int deplacement_y, int speed, char * sprite, int display, int plan){

  Element * platform = createImage(x,y,longueur,hauteur,sprite,display,plan);

  platform_t * structPlatform = initDataPlatform(player,speed,x,y,x + deplacement_x,y + deplacement_y);

  setDataElement(platform,(void *)structPlatform);

  switch (option){
    //mouvante
    case 1:
    setActionElement(platform,mooving_platform);
    break;
    //déplaçable
    case 2:
    setActionElement(platform,interactive_platform);
    break;
  }

  return platform;
}

void mooving_platform(Element * platform){

  platform_t * structPlatform;
  float xplatform, yplatform, wplatform, hplatform;
  float vect_x, vect_y;
  int temp;

  getDataElement(platform,(void **)&structPlatform);
  getCoordElement(platform,&xplatform,&yplatform);
  getDimensionElement(platform,&wplatform,&hplatform);

  vect_x = (float)(structPlatform->xarrive - structPlatform->xdepart)/sqrt(pow(structPlatform->xarrive - structPlatform->xdepart,2) + pow(structPlatform->yarrive - structPlatform->ydepart,2));
  vect_y = (float)(structPlatform->yarrive - structPlatform->ydepart)/sqrt(pow(structPlatform->xarrive - structPlatform->xdepart,2) + pow(structPlatform->yarrive - structPlatform->ydepart,2));

  //printf("%f %f\n",vect_x,vect_y);

  if(xplatform >= structPlatform->xarrive - abs(structPlatform->speed) && xplatform <= structPlatform->xarrive + abs(structPlatform->speed) && yplatform >= structPlatform->yarrive - abs(structPlatform->speed) && yplatform <= structPlatform->yarrive + abs(structPlatform->speed)){
    temp = structPlatform->xdepart;
    structPlatform->xdepart = structPlatform->xarrive;
    structPlatform->xarrive = temp;

    temp = structPlatform->ydepart;
    structPlatform->ydepart = structPlatform->yarrive;
    structPlatform->yarrive = temp;
    //printf("changement\n");
  }

  moveElement(platform,structPlatform->speed*vect_x,structPlatform->speed*vect_y);
  //moveElement(structPlatform->player,structPlatform->speed*vect_x,structPlatform->speed*vect_y);

}

void interactive_platform(Element * platform){

  perso_t * structPerso;
  platform_t * structPlatform;
  float xperso, yperso, wperso, hperso;
  float xplatform, yplatform, wplatform, hplatform;

  getDataElement(platform,(void **)&structPlatform);
  getCoordElement(platform,&xplatform,&yplatform);
  getDimensionElement(platform,&wplatform,&hplatform);

  getDataElement(structPlatform->player,(void **)&structPerso);
  getCoordElement(structPlatform->player,&xperso,&yperso);
  getDimensionElement(structPlatform->player,&wperso,&hperso);

  if(structPerso->action && xperso + wperso >= xplatform - 0.5*wperso && xperso <= xplatform + wplatform + 0.5*wperso && yperso + hperso >= yplatform - 0.5*hperso && yperso <= yplatform + hplatform + 0.5*hperso){
    if(structPerso->sac->objet == NULL){

      structPerso->sac->objet = platform;

    }else{

      structPerso->sac->objet = NULL;

    }
  }


}

void createDoor(Element * player, int xdepart, int ydepart, int xarrive, int yarrive, int longueur, int hauteur, int displayDepart, int planDepart, int displayArrive, int planArrive, char * sprite){

  Element * doorIn = createImage(xdepart, xarrive, longueur, hauteur, sprite, displayDepart, planDepart);
  Element * doorOut = createImage(xarrive, yarrive, longueur, hauteur, sprite, displayArrive, planArrive);

  door_t * structDoorIn = malloc(sizeof(door_t));
  door_t * structDoorOut = malloc(sizeof(door_t));

  structDoorIn->player = player;
  structDoorIn->xarrive = xarrive;
  structDoorIn->yarrive = yarrive;
  structDoorIn->displayArrive = displayArrive;

  structDoorOut->player = player;
  structDoorOut->xarrive = xdepart;
  structDoorOut->yarrive = ydepart;
  structDoorOut->displayArrive = displayDepart;

  setDataElement(doorIn, (void *)structDoorIn);
  setDataElement(doorOut, (void *)structDoorOut);

  setActionElement(doorIn, tp);
  setActionElement(doorOut, tp);

}

void tp(Element * door){

  door_t * structDoor;
  float xdoor, ydoor, wdoor, hdoor;
  float xplayer, yplayer, wplayer, hplayer;

  getDataElement(door, (void **)&structDoor);

  getCoordElement(door, &xdoor, &ydoor);
  getDimensionElement(door, &wdoor, &hdoor);

  getCoordElement(structDoor->player, &xplayer, &yplayer);
  getDimensionElement(structDoor->player, &wplayer, &hplayer);

  if(xplayer + wplayer >= xdoor && xplayer <= xdoor + wdoor && yplayer + hplayer >= ydoor && yplayer <= ydoor + hdoor){
    setDisplayCodeWindow(structDoor->displayArrive);
    setDisplayCodeElement(structDoor->player,1,1);
    replaceElement(structDoor->player, xdoor, yplayer - 10);
  }

}
